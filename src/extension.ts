import * as net from 'net';
import * as path from 'path';
import { ExtensionContext, window, commands, workspace, debug, Uri, DebugConfiguration, WorkspaceFolder } from 'vscode';
import {
    LanguageClient,
    LanguageClientOptions,
    StreamInfo,
    CloseAction,
    ErrorAction,
    Message,
    Executable,
    ExecutableOptions,
    RevealOutputChannelOn,
    ServerOptions
} from 'vscode-languageclient';
import * as requirements from './requirements';

let client: LanguageClient;

export function activate(context: ExtensionContext) {
    if(!validationEnabled()){
        return;
    }
    var port = readPort();
    if (port) {
        startRemoteServer(context, port);
    } else {
        startLocalServer(context);
    }
    context.subscriptions.push(commands.registerCommand("d3e.generate.debug", async (uri: Uri) => {
        await attachToDebugger(uri, false);
    }));
}

function attachToDebugger(uri: Uri, noDebug: boolean){
    var port = readPort();
    const debugConfig: DebugConfiguration = {
        type: 'd3e',
        name:'D3E',
        request: 'attach',
        debugServer: 9877,
    };

    return debug.startDebugging(undefined, debugConfig);
}

function readPort(): number {
    const config = workspace.getConfiguration();
    return config.get<number>('server.port', 0);
}
function validationEnabled(): boolean {
    const config = workspace.getConfiguration();
    return config.get<boolean>('validate', true);
}
function startLanguageClient(context: ExtensionContext, serverOptions: ServerOptions) {
    // Options to control the language client
    let clientOptions: LanguageClientOptions = {
        // Register the server for plain text documents
        documentSelector: [{ scheme: 'file', language: 'd3e' }, { scheme: 'file', language: 'd3et' }],
        errorHandler: {
            error: (error: Error, message: Message, count: number) => {
                console.log("Error: " + error.message);
                return ErrorAction.Continue;
            },
            closed: () => CloseAction.Restart
        },
        revealOutputChannelOn: RevealOutputChannelOn.Info
    };

    // Create the language client and start the client.
    client = new LanguageClient(
        'd3eLanguageServer',
        'D3E Language Server',
        serverOptions,
        clientOptions
    );
    client.registerProposedFeatures();
    // Start the client. This will also launch the server
    context.subscriptions.push(client.start());
}

function startRemoteServer(context: ExtensionContext, port: number) {
    startLanguageClient(context, () => prepareRemoteServer(port));
}

function prepareRemoteServer(port: number): Promise<StreamInfo> {
    const socket = net.connect(port, "localhost", () => {
        console.log('Connection local address : ' + socket.localAddress + ":" + socket.localPort);
        console.log('Connection remote address : ' + socket.remoteAddress + ":" + socket.remotePort);
    });
    socket.on("error", (err) => {
        console.log(err);
    })
    const result: StreamInfo = {
        writer: socket,
        reader: socket
    };
    return Promise.resolve(result);
}

function startLocalServer(context: ExtensionContext) {
    requirements.resolveRequirements().catch(error => {
        // show error
        window.showErrorMessage(error.message, error.label).then((selection) => {
            if (error.label && error.label === selection && error.command) {
                commands.executeCommand(error.command, error.commandParam);
            }
        });
        // rethrow to disrupt the chain.
        throw error;
    }).then(req => startLanguageClient(context, prepareServerRequirments(context, req)));
}
function prepareServerRequirments(context: ExtensionContext, req: requirements.RequirementsData): Executable {
    const executable: Executable = Object.create(null);
    const options: ExecutableOptions = Object.create(null);
    options.env = process.env;
    options.stdio = 'pipe';
    options.cwd = path.resolve(context.extensionPath, './server');
    executable.options = options;
    executable.command = path.resolve(req.java_home + '/bin/java');
    executable.args = ['-jar', 'd3e-langserver.jar'];
    console.log(`Starting Java server with: ${executable.command} ${executable.args.join(' ')}`);
    console.log(options.cwd);
    console.log(__dirname);
    return executable;
}

export function deactivate(): Thenable<void> | undefined {
    if (!client) {
        return undefined;
    }
    return client.stop();
}
