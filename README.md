# d3e-vscode README

D3E VSCode extension allows developers to develop D3E code.

## Features

Validation
Formatting
AutoComplete

## Requirements

You need valid JDK 12

## Extension Settings

This extension contributes the following settings:

* `java.home`: Valid Java 11+ home

## Known Issues



## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Release with Syntax Heighlite support and validation.
